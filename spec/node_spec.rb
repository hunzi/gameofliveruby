# A Node is a uniqe entity, that is connected to "neighbors", thus creating a network
require './src/node'

RSpec.describe Map::Node, '#new' do
    context 'with no arguments' do
        it 'raises an ArgumentError' do
            expect{Map::Node.new}.to raise_exception ArgumentError
        end
    end

    context 'with an integer as argument' do
        it 'returns an instance of Map::Node' do
            node = nil
            expect{ node = Map::Node.new(1)}.not_to raise_exception
            expect(node.is_a?(Map::Node)).to be(true)
        end
    end

    context 'with an string as argument' do
        it 'returns an instance of Map::Node' do
            node = nil
            expect{ node = Map::Node.new("N1")}.not_to raise_exception
            expect(node.is_a?(Map::Node)).to be(true)
        end
    end
end

RSpec.describe Map::Node, '#id' do
    it 'returns the id that was handed as parameter' do
        id = 123
        node = Map::Node.new(id)
        expect(node.id).to eq(id)
    end

    it 'cannot be used to change the value' do
        node = Map::Node.new(123)
        expect{node.id = 3}.to raise_exception NoMethodError
    end
end

RSpec.describe Map::Node, '#neighbors' do
    it 'returns an array of neighbors' do
        
    end
end