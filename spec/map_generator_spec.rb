# The map generator creates and interconnects an array of nodes
# After that, it stores them into a map-collection
require './src/generator'
require './src/collection'
require './src/node'

RSpec.describe Map, '#create' do
    context 'create a map of 8x8 nodes' do
        it 'returns an instance of Map::Collection' do
            expect(Map.create(8, 8)).to be_a(Map::Collection)
        end

        context 'the nodes are correctly interconnected' do
            it 'the 1st node is connected to 2, 9, 10' do
                map = Map.create(8, 8)
                node = map.get(1)
                expect(node.neighbors).to eq([2, 9, 10])
            end

            it 'the 2nd node is connected to 1, 3, 9, 10, 11' do
                map = Map.create(8, 8)
                node = map.get(2)
                expect(node.neighbors).to eq([1, 3, 9, 10, 11])
            end

            it 'the 8th node is connected to 7, 15, 16' do
                map = Map.create(8, 8)
                node = map.get(8)
                expect(node.neighbors).to eq([7, 15, 16])
            end

            it 'the 9th node is connected to 1, 2, 10, 17, 18' do
                map = Map.create(8, 8)
                node = map.get(9)
                expect(node.neighbors).to eq([1, 2, 10, 17, 18])
            end

            it 'the 10th node is connected to 1, 2, 3, 9, 11, 17, 18, 19' do
                map = Map.create(8, 8)
                node = map.get(10)
                expect(node.neighbors).to eq([1, 2, 3, 9, 11, 17, 18, 19])
            end

            it 'the 16th node is connected to 7, 8, 15, 23, 24' do
                map = Map.create(8, 8)
                node = map.get(16)
                expect(node.neighbors).to eq([7, 8, 15, 23, 24])
            end

            it 'the 57th node is connected to 49, 50, 58' do
                map = Map.create(8, 8)
                node = map.get(57)
                expect(node.neighbors).to eq([49, 50, 58])
            end

            it 'the 58th node is connected to 49, 50, 51, 57, 59' do
                map = Map.create(8, 8)
                node = map.get(58)
                expect(node.neighbors).to eq([49, 50, 51, 57, 59])
            end

            it 'the 64th node is connected to 55, 56, 63' do
                map = Map.create(8, 8)
                node = map.get(64)
                expect(node.neighbors).to eq([55, 56, 63])
            end
        end
    end
end