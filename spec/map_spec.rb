# A Map is a collection to store an Array of "Nodes"
# It offers an interface to interact with these nodes
require './src/collection'
require './src/node'

RSpec.describe Map::Collection, '#new' do
    context 'with no arguments' do
        it 'throws an error' do
            expect{Map::Collection.new}.to raise_error ArgumentError
        end
    end

    context 'with an array of Map::Node as argument' do
        it 'raises an ArgumentError' do
            nodes = Array.new(5) do |i|
                Map::Node.new i+1
            end
            expect{Map::Collection.new nodes}.to raise_error ArgumentError
        end
    end

    context 'with and array of Map::Node an a row-width as arguments' do
        context 'where every node has a unique id' do
            it 'creates an instance of map' do
                nodes = Array.new(5) do |i|
                    Map::Node.new i+1
                end
                expect{Map::Collection.new nodes, 5}.not_to raise_error
                map = Map::Collection.new nodes, 5
                expect(map.is_a?(Map::Collection))
            end
        end

        context 'where some nodes share the same id' do
            it 'raises an ArgumentError' do
                nodes = Array.new(5) do |i|
                    Map::Node.new (i/2).ceil
                end
                expect{Map::Collection.new nodes, 5}.to raise_error ArgumentError, 'FUC'
            end
        end

        context 'where not all nodes have an id' do
            class MockNodeNoId
                attr_reader :neighbors
            end
            it 'raises an ArgumentError' do
                nodes = Array.new(5) do |i|
                    if i%2 === 0
                        Map::Node.new (i/2).ceil
                    else
                        MockNodeNoId.new
                    end 
                end
                expect{Map::Collection.new nodes, 5}.to raise_error ArgumentError, 'FUC'
            end
        end

        context 'where some nodes do not have an neighbor-attribute' do
            class MockNodeNoNeighbors
                attr_reader :id
            end

            it 'raises an ArgumentError' do
                nodes = Array.new(5) do |i|
                    if i%2 === 0
                        Map::Node.new (i/2).ceil
                    else
                        MockNodeNoNeighbors.new
                    end 
                end
                expect{Map::Collection.new nodes, 5}.to raise_error ArgumentError, 'FUC'
            end
        end

        context 'where type of id switches between different types' do
            it 'raises an ArgumentError' do
                nodes = Array.new(5) do |i|
                    if i%2 === 0
                        Map::Node.new (i)
                    else
                        Map::Node.new ('N'<<i)
                    end 
                end
                expect{Map::Collection.new nodes, 5}.to raise_error ArgumentError, 'FUC'
            end
        end
    end
end

RSpec.describe Map::Collection, '#get(id)' do
    context 'with nodes that use integer as id\'s' do
        context 'with an integer as argument' do
            nodes = Array.new(5) do |i|
                Map::Node.new(i+1)
            end
            it 'returns the instance with the corresponding id' do
                map = Map::Collection.new(nodes, 5)
                expect(map.get(1)).to be(nodes[0])
            end
        end
        context 'with an string as argument' do
            nodes = Array.new(5) do |i|
                Map::Node.new(i+1)
            end
            it 'raises an argument error' do
                map = Map::Collection.new(nodes, 5)
                expect{map.get("N1")}.to raise_exception ArgumentError, 'FFF'
            end
        end
    end

    context 'with nodes that use strings as id\'s' do
        context 'with an integer as argument' do
            it 'raises an argument error' do
                nodes = Array.new(5) do |i|
                    Map::Node.new('N' << (i+1).to_s)
                end
                map = Map::Collection.new(nodes, 5)
                expect{map.get(1)}.to raise_exception ArgumentError, 'FFF'
            end
        end
        context 'with an string as argument' do
            it 'returns the instance with the corresponding id' do
                nodes = Array.new(5) do |i|
                    Map::Node.new('N' << (i+1).to_s)
                end
                map = Map::Collection.new(nodes, 5)
                expect(map.get("N1")).to be(nodes[0])
            end
        end
    end
end

RSpec.describe Map::Collection, '#find_neighbors' do

end