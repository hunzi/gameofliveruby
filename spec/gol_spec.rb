require './src/gameoflife'

RSpec.describe GoL, '#turn' do
    context '8x8 Map' do
        context 'Any live cell with fewer than two live neighbours dies, as if by underpopulation.' do
            it 'a single alive node dies' do
                map = createMap()
                map.get(10).alive=true
                expect(map.nr_alive()).to eq(1)
                expect(GoL.turn(map).nr_alive()).to eq(0)
            end

            it 'two nodes adjecent to each other die' do
                map = createMap()
                (10..11).each do |id|
                    map.get(id).alive=true
                end
                expect(map.nr_alive()).to eq(2)
                expect(GoL.turn(map).nr_alive()).to eq(0)
            end

            it 'also dies when diagonal' do
                map = createMap()
                [1, 10].each do |id|
                    map.get(id).alive=true
                end
                expect(map.nr_alive()).to eq(2)
                expect(GoL.turn(map).nr_alive()).to eq(0)
            end

            it 'every second node in every second row - all dies' do
                map = createMap()
                map.nodes.each_index do |i|
                    next if(i > 7 && (i/8).floor%2!=0)
                    map.nodes[i].alive = true if i%2 === 0
                end
                expect(map.nr_alive()).to eq(16)
                expect(GoL.turn(map).nr_alive()).to eq(0)
            end
        end

        context 'Any live cell with two or three live neighbours lives on to the next generation.' do
            it 'a row of three nodes the center survives' do
                map = createMap()
                (1..3).each { |id| map.get(id).alive = true }
                expect(map.nr_alive()).to eq(3)
                map = GoL.turn(map)
                expect(map.get(2).alive).to be(true)
            end

            # it 'every second node alive, 15 survive' do
            #     map = createMap()
            #     (10..12).each do |id|
            #         map.get(id).alive=true
            #     end
            #     expect(map.nr_alive()).to eq(3)
            #     expect(GoL.turn(map).nr_alive()).to eq(1)
            #     expect(map.get(11).alive).to be(true)
            # end

            it 'a block of four all survive' do
                map = createMap()
                [1, 2, 9, 10].each { |id| map.get(id).alive = true }
                expect(map.nr_alive()).to eq(4)
                map = GoL.turn(map)
                expect(map.nr_alive()).to eq(4)
            end
        end

        context 'Any live cell with more than three live neighbours dies, as if by overpopulation.' do
            it 'a block of five, six survive' do
                map = createMap()
                [10, 11, 12, 18, 19].each { |id| map.get(id).alive = true }
                expect(map.nr_alive()).to eq(5)
                map = GoL.turn(map)
                expect(map.nr_alive()).to eq(5)
                [11, 19].each { |id| expect(map.get(id).alive).to be(false) }
                [3, 10, 12, 18, 20].each { |id| expect(map.get(id).alive).to be(true) }
            end
        end

        context 'Any dead cell with three live neighbours becomes a live cell, as if by reproduction.' do
            it 'IT LIVES' do
                map = createMap()
                parents = [1, 3, 18]
                parents.each { |id| map.get(id).alive = true }
                expect(map.nr_alive()).to eq(3)
                map2 = GoL.turn(map)
                expect(map2.nr_alive()).to eq(1)
                expect(map2.get(10).alive).to be(true)
                parents.each { |id| expect(map2.get(id).alive).to be(false) }
            end
        end
    end
end

def createMap
    nodes = Array.new(64) do |i|
        id = i+1
        neighbors = Map.interconnect(id, 8, 64)
        GoL::Node.new(id, neighbors, false)
    end
    GoL::Collection.new(nodes, 64)
end