require './src/generator'

module GoL
    def GoL.setup(row_len, col_len)
        nr_nodes = col_len * row_len
        nodes = Array.new(nr_nodes) do |i|
            id = i+1
            neighbors = Map.interconnect(id, row_len, nr_nodes)
            Node.new(id, neighbors, rand(8)%7===0)
        end
        Map::Collection.new(nodes, row_len)
    end

    def GoL.turn(map)
        newMap = []
        map.nodes.each do |node|
            alive_neighbors = 0
            node.neighbors.each do |id|
                if map.get(id).alive
                    alive_neighbors += 1
                end
            end
            if node.alive
                if alive_neighbors > 1 && alive_neighbors < 4
                    newMap.push(GoL::Node.new(node.id, node.neighbors, true))
                else
                    newMap.push(GoL::Node.new(node.id, node.neighbors, false))
                end
            else
                if alive_neighbors === 3
                    newMap.push(GoL::Node.new(node.id, node.neighbors, true))
                else
                    newMap.push(GoL::Node.new(node.id, node.neighbors, false))
                end
            end
        end
        return GoL::Collection.new(newMap, map.row_len)
    end

    class Collection < Map::Collection
        def nr_alive
            alive = @nodes.filter { |node| node.alive }
            alive.length
        end
    end

    class Node < Map::Node
        attr_accessor :alive

        def initialize(id, neighbors, alive = false)
            super(id, neighbors)
            @alive = alive
        end
    end
end