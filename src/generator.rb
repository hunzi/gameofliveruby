require './src/node'
require './src/collection'

module Map
    def Map.create(col_len, row_len)
        nr_nodes = col_len * row_len
        nodes = Array.new(nr_nodes) do |i|
            id = i+1
            neighbors = Map.interconnect(id, row_len, nr_nodes)
            Node.new(id, neighbors)
        end
        Map::Collection.new(nodes, row_len)
    end

    def Map.interconnect id, row_len, max_nodes
        if id < 1 then raise ArgumentError.new('Id has to start with 1') end
        neighbors = []
        if id > row_len
            neighbors << id - row_len - 1 if (id-1)%row_len != 0
            neighbors << id - row_len
            neighbors << id - row_len + 1 if id%row_len != 0
        end
        neighbors << id - 1 if (id-1)%row_len != 0
        neighbors << id + 1 if id%row_len != 0
        if id < max_nodes-row_len
            neighbors << id + row_len - 1 if (id-1)%row_len != 0
            neighbors << id + row_len
            neighbors << id + row_len + 1 if id%row_len != 0
        end
        neighbors
    end
end