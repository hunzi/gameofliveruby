module Map
    class Node
        attr_reader :id, :neighbors
        def initialize id, neighbors = []
            @id = id
            @neighbors = neighbors
        end
    end
end