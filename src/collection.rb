module Map
    class Collection
        attr_reader :nodes, :row_len
        def initialize nodes, row_len
            if !check_nodes(nodes)
                raise ArgumentError.new 'FUC'
            end
            @nodes = nodes
            @row_len = row_len
        end

        def get id
            raise ArgumentError.new('FFF') if !id.is_a?(@id_type)
            @nodes.each do |node|
                return node if node.id === id
            end
        end

        private
        def check_nodes nodes
            nodes.each do |node|
                begin
                    node.id
                    node.neighbors
                rescue
                    return false
                end
            end
            ids = []
            @id_type = nodes[0].id.class
            nodes.each do |node|
                if ids.include?(node.id) || !node.id.is_a?(@id_type)
                    return false
                end
                ids << node.id
            end
            true
        end
    end
end