require 'gosu'
require './src/gameoflife'

ENV['PATH'] = File.join("..", "src") + ";" + ENV['PATH']

class App < Gosu::Window
    def initialize width, height
        super(width, height)
        @ticks = 0
        @map = GoL.setup(20,15)
        @tiles = Gosu::Image.load_tiles('assets\dg_dungeon32.gif', 32, 32)
    end

    def update
        if @ticks == 30
            @map = GoL.turn(@map)
            @ticks = 0
        else
            @ticks += 1
        end
    end

    def draw
        @map.nodes.each do |node|
            base = node.id-1
            y = ((base/@map.row_len).floor)
            x = (base - y*@map.row_len)*32
            if node.alive
                @tiles[63].draw x, y*32, 0
            else
                @tiles[0].draw x, y*32, 0
            end
        end
    end

    def button_down(id)
        if Gosu::KB_ESCAPE === id
            close
        elsif Gosu::KB_SPACE === id
            @map = GoL.setup(20,15)
        else
            super
        end
    end
end

App.new(640, 480).show