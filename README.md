# Implementation of Conway's Game of Life

[Learn More](https://en.wikipedia.org/wiki/Conway's_Game_of_Life)

## Rules

1. Any live cell with fewer than two live neighbours dies, as if by underpopulation.
2. Any live cell with two or three live neighbours lives on to the next generation.
3. Any live cell with more than three live neighbours dies, as if by overpopulation.
4. Any dead cell with three live neighbours becomes a live cell, as if by reproduction.

## Controlls

* `space` will create a new seed.

## Play

### Dependencies

* For additional Linux dependencies [look here](https://github.com/gosu/gosu/wiki/Getting-Started-on-Linux).
* Ruby >2.3

### Install

#### CMD

```Bash
bundle install
```

#### Windows Executable

```PowerShell
bundle install
bundle exec rake build
```

### Start

Entweder

```Bash
ruby src/app.rb
```

Oder Doppelclick auf

```Bash
pkg/game_of_live_1_0_0_WIN32.exe
```
